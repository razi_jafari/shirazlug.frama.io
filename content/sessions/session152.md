---
title: "جلسه ۱۵۲"
description: "غلط‌یابی و استانداردسازی متن فارسی"
date: "1397-06-26"
author: "گودرز جعفری"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster152.jpg"
---
[![poster152](../../img/posters/poster152.jpg)](../../img/poster152.jpg)
